//============================================================================
// Name        : yxa-tcp-gate-example.cpp
// Author      : EvdokimovYV
// Version     :
// Copyright   : GPLv3
// Description : Simple TCP-gate example based on yxa::netw
//============================================================================

#include <iostream>
#include <list>
#include <array>
#include <functional>
#include <arpa/inet.h>
#include <signal.h>
#include "yxa-netw.hpp"
using namespace std;

using nt_sck = yxa::net::sock;
using nt_sslctr = nt_sck::selector;

class sock_link:public nt_sck::user_handling_obj{
public:
	typedef std::list<sock_link> list_t;
private:
	nt_sslctr &mslctr;
	nt_sck &m_in, &m_out;
public:
	list_t::iterator it;

	sock_link(nt_sck &ain, nt_sck &aout, nt_sslctr &aslctr)
									: mslctr(aslctr), m_in(ain), m_out(aout){}
	void send_to_another(nt_sck &asck, nt_sck::data &adt){
		((asck == m_in)? m_out: m_in) << adt;
	}
	void close_another(nt_sck &asck){mslctr.remove((asck == m_in)? m_out: m_in);}
};
//------------------------------------------------------------------------------
class px_srv:	public nt_sck::server,		public nt_sck::server::acceptor,
				public nt_sck::receiver,	public nt_sck::except_handler{
	ITS_NO_STUB
	in_addr_t m_rmt_IP;
	in_port_t m_rmt_prt;
	nt_sslctr &mSlctr;
	sock_link::list_t m_links;
public:
	px_srv(in_addr_t armtIP, in_port_t armtPrt, nt_sck::selector &aslctr)
						:m_rmt_IP(armtIP), m_rmt_prt(armtPrt), mSlctr(aslctr){}
	//as server
	nt_sck::server::acceptor& accept_by(in_addr_t remoteIP){return *this;}
	void on_accepted(in_addr_t, in_port_t, nt_sck::pointer &psck){

		auto cc = mSlctr.add_TCP_client(m_rmt_IP, m_rmt_prt, *this, *this);

		if (!cc){
			mSlctr.remove(*psck);
			return;
		}
		m_links.push_back(sock_link(*psck, *cc, mSlctr));

		auto it = m_links.end();

		it->it = --it;
		psck->attach(*it);
		cc->attach(*it);
	}
	//as server::acceptor
	nt_sck::receiver&	receive_async_by(){return *this;}
	//as receiver
	bool handle_rcv(nt_sck &asck, nt_sck::data &dt, in_addr_t ip, in_port_t){

		dynamic_cast<sock_link&>(asck.attached()).send_to_another(asck, dt);
		return true;
	}
	//as socket exception handler
	void handle_exc(nt_sck &asck, exc_cd acd){
//		if (yxa::net::sock::except_handler::exc_discnnect != acd)
//			return;

		auto &rlnk = dynamic_cast<sock_link&>(asck.attached());

		rlnk.close_another(asck);
		m_links.erase(rlnk.it);
	}
};
//==============================================================================
static class: public nt_sck::selector::manager{
public:
	bool miCnCnt = true;
	bool isCanContinue(){return miCnCnt;}
}gIntrptr;
//------------------------------------------------------------------------------
static void ths_sig_int(int sig_num){gIntrptr.miCnCnt = false;}
//==============================================================================
static const size_t gMxCnnctns = 16;
static void info_using_out(const char *excobj){
	std::cout	<< std::endl << "Use: " << excobj
				<< " {local_port remote_IP_address [remote_port "
				<< "[max_client_connections]]}"
				<< std::endl << std::endl << "note:" << std::endl
				<< std::endl << "max_client_connections: default - "
				<< gMxCnnctns << std::endl;
}
static bool is_ok_val(const char *arg, uint16_t &avl){
	try {
		auto vvl = std::atoi(arg);

		if ((0 < vvl) && (0xFFFF >= vvl)){
			avl = vvl;
			return true;
		}
	}catch (std::invalid_argument&) {}
	return false;
}
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	if (3 > argc){
		std::cout << std::endl << "Local port & remote IP not defined!" << std::endl;
		info_using_out(argv[0]);
		return -1;
	}
	in_port_t vlp = 0, vrp = 0;
	uint16_t vmxcnn = gMxCnnctns;
	in_addr_t vIP = 0;

	std::array<std::pair<const char*, std::function<bool()>>, 4> args = {{
		{"local port",	[&vlp, &vrp, argv](){
												if (!is_ok_val(argv[1], vlp))
													return false;
												vrp = vlp;//use remote as local if not defined above
												return true;
											}
		},
		{"remote IP address",
						[&vIP, argv](){return 0 != (vIP = inet_addr(argv[2]));}},
		{"remote port",	[&vrp, argv](){return is_ok_val(argv[3], vrp);}},
		{"maximum client connections",
						[&vmxcnn, argv](){return is_ok_val(argv[4], vmxcnn);}}
	}};

	if (argc > 5)
		argc = 5;

	--argc;

	for (int i = 0; i < argc; ++i){

		auto &rc = args.at(i);

		if (rc.second())
			continue;

		std::cout << "Invalid " << rc.first << " value!" << std::endl;
		info_using_out(argv[0]);
		return -1;
	}
	auto pSlctr = nt_sck::selector::create();
	px_srv vPrx(vIP, vrp, *pSlctr);

	pSlctr->add_TCP_server(vlp, vmxcnn, vPrx, vPrx);
	signal(SIGINT, ths_sig_int);
	pSlctr->process(gIntrptr);
	return 0;
}
//==============================================================================
