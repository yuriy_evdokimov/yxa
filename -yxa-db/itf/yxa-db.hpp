/*	Y-eXtensible Architecture
 *
 * yxa-db.hpp
 *
 *  Created on: 13 feb 2016.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */

#pragma once
#include <list>
#include "yxa-itr-def.h"

namespace yxa{namespace db{

INTERFACE(table)

	INTERFACE(row)

		INTERFACE(value)
			OPERATOR(,int,,const)
			OPERATOR(,std::string,,const)
			OPERATOR(,bool,, const)

			OPERATOR(void, =, const char*)
			OPERATOR(void, =, int)
			OPERATOR(void, =, bool)
		INTRF_END

		OPERATOR(const value&, [], const char *column, const)
	INTRF_END

	INTERFACE(column)

		INTERFACE(condition)
			CONST_SELF_OPERATOR(&&, const condition&)
			CONST_SELF_OPERATOR(||, const condition&)
		INTRF_END

		OPERATOR(const condition&, ==, const table::column&,const)
		OPERATOR(const condition&, !=, const table::column&,const)
		OPERATOR(const condition&, ==, const char *arg,		const)
		OPERATOR(const condition&, !=, const char *arg,		const)
		OPERATOR(const condition&, <=, const char *arg,		const)
		OPERATOR(const condition&, >=, const char *arg,		const)
		CONST_METHOD(const condition& roughly(const char *arg))
//			CONST_METHOD(const condition& between(const char *amn, const char *amx))
		OPERATOR(const condition&, ==, int arg,				const)
		OPERATOR(const condition&, !=, int arg,				const)
		OPERATOR(const condition&, ==, bool,				const)
		HAVE_STUB(any)
	INTRF_END

//	OPERATOR(const row&, [], int, const)
	OPERATOR(const column&, [], const char *column, const)
INTRF_END

/*namespace SQL{
	INTERFACE(conditional)
		METHOD(void where(const table::column::condition&))
	INTRF_END

	INTERFACE(meaning)
		OPERATOR(table::row&, [], const char*)
	INTRF_END

	INTERFACE_SH_FROM(select, conditional, _())
		METHOD(const table& from(const char*))
	INTRF_END

	INTERFACE(command)
	INTRF_END

	INTERFACE_FROM(insertion, command)
	INTRF_END

	INTERFACE_FROM2(_update, command, conditional)
	INTRF_END

	INTERFACE_FROM2(_delete, command, conditional)
	INTRF_END
}

INTERFACE(rq_result)
	CAN_USE_BY_POINTER

	OPERATOR(const table::row&, [], int, const)
INTRF_END

INTERFACE(_connection)
	CAN_USE_BY_POINTER

	static pointer postgr_open(const char *db_name,
						const char *login, const char *passw,
						const char *host = nullptr, unsigned short port = 5432);

	METHOD(rq_result::pointer request(SQL::select&))
	METHOD(bool execute(SQL::command&))
INTRF_END*/
//------------------------------------------------------------------------------

INTERFACE(selection)
	CAN_USE_BY_POINTER

	INTERFACE(row)
		CAN_USE_BY_POINTER

		INTERFACE(value)
			WITH_ENUM(_type, _string, _integer, _date_time, _bool)
			OPERATOR(,int,,const)
			OPERATOR(,double,,const)
			OPERATOR(,std::string,,const)
			OPERATOR(,bool,, const)
		INTRF_END

		OPERATOR(const value&, [], const char *column, const)
	INTRF_END

	IT_CONTAINS_CNT(row)
	OPERATOR(row::pointer, [], size_t, const)
INTRF_END

INTERFACE(request)
	CAN_USE_BY_POINTER

	INTERFACE(condition)
		CONST_SELF_OPERATOR(&&, const condition&)
		CONST_SELF_OPERATOR(||, const condition&)
	INTRF_END

	INTERFACE(table)
		INTERFACE(column)

			OPERATOR(const condition&, ==, const table::column&,const)
			OPERATOR(const condition&, !=, const table::column&,const)
			OPERATOR(const condition&, ==, const char *arg,		const)
			OPERATOR(const condition&, !=, const char *arg,		const)
			OPERATOR(const condition&, <=, const char *arg,		const)
			OPERATOR(const condition&, >=, const char *arg,		const)
			CONST_METHOD(const condition& roughly(const char *arg))
//			CONST_METHOD(const condition& between(const char *amn, const char *amx))
			OPERATOR(const condition&, ==, int arg,				const)
			OPERATOR(const condition&, !=, int arg,				const)
			OPERATOR(const condition&, ==, bool,				const)
			HAVE_STUB(any)
		INTRF_END

		INTERFACE(row)
			INTERFACE(value)
				OPERATOR(void, =, const char*)
				OPERATOR(void, =, int)
				OPERATOR(void, =, bool)
			INTRF_END

			OPERATOR(value&, [], const char*)
		INTRF_END

		OPERATOR(const column&, [], const char *column, const)
		METHOD(row& prepare_row())
		METHOD(void add(row&))
		METHOD(void update(row&))
	INTRF_END

	METHOD(table& use_table(const char *name))
	OPERATOR(void, =, const condition&)
	OPERATOR(void, &=, const condition&)
	OPERATOR(void, |=, const condition&)
	METHOD(void need_out(const table::column&, const char *alias = nullptr))//only for custom out
	METHOD(void limited(size_t aCount, size_t aOffs = 0));
	METHOD(void order_by(const table::column&))
	METHOD(void need_out_fnc(const char *fnc,
								const table::column &c = table::column::any()))

	INTERFACE(Union)
		SELF_OPERATOR(+=, const request&)
		METHOD(void limited(size_t aCount, size_t aOffs = 0))
		METHOD(void sort_by(size_t))
	INTRF_END

	OPERATOR(Union&, +, const request&, const)
INTRF_END

INTERFACE_SH(connection, open(	const char *db_name,
						const char *login, const char *passw,
						const char *host = nullptr, unsigned short port = 5432))

	METHOD(request::pointer create_request(const char *db_foldr = nullptr))

	METHOD(selection::pointer select(const char *table,
												std::list<const char*> columns))
	METHOD(selection::pointer select(const request&))

	METHOD(selection::pointer select(const request::Union&))
INTRF_END
}}
