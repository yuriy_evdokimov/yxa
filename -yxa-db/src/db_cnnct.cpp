/*	Y-eXtensible Architecture
 *
 * db_cnnct.cpp
 *
 *  Created on: 13 feb 2016.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */
#include <map>
#include <list>
#include <mutex>
#include <sstream>
//#include <iostream>
#include <cstring>
#include "yxa-db.hpp"
#include <postgresql/libpq-fe.h>

namespace yxa{ namespace db{

typedef enum {_equal, _unequal, _less, _more, _eqless, _eqmore, _prdct_cnt}_prdct_t;

static const char* gsEqn[_prdct_cnt] = {
	[_equal] = " = ",		[_unequal] = " <> ",	[_less] = " < ",
	[_more] = " > ",		[_eqless] = " <= ",		[_eqmore] = " >= "
};
class pre_rq_clmn: public request::table::column{//column request prototype
public:
	std::string m_nm;

	virtual std::string get_str(bool wthtbl) const = 0;
	virtual bool operator < (const pre_rq_clmn& othr) const = 0;
	virtual ~pre_rq_clmn(){}

	class ref{
	public:
		const pre_rq_clmn &mrc;
		ref(const pre_rq_clmn &ac):mrc(ac){}
		bool operator < (const ref& othr) const{return mrc < othr.mrc;}
	};

};
static const char *gExcMsg = "For any column condition";

static class: public pre_rq_clmn{
	ITS_STUB
	std::string get_str(bool wthtbl) const{return "*";}
	bool operator < (const pre_rq_clmn& othr) const{return true;}
	const request::condition& operator == (const request::table::column &acl) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator != (const request::table::column &acl) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator == (const char *arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator != (const char *arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator <= (const char *arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator >= (const char *arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& roughly(const char *arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator == (int arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator != (int arg) const{
		throw std::logic_error(gExcMsg);
	}
	const request::condition& operator == (bool arg) const{
		throw std::logic_error(gExcMsg);
	}
}_stub_clmn;
//------------------------------------------------------------------------------
class in_cndt: public request::condition{
public:
	typedef std::shared_ptr<in_cndt> ptr;
protected:
	typedef enum{_prdct, _not, _and, _or} _tp;

	_tp m_tp = _prdct;
	const char *msCmp = nullptr;
	mutable ptr m_crtd;
	const in_cndt *m_rl[2] = {nullptr};
	const pre_rq_clmn &slfclmn;

	in_cndt(const pre_rq_clmn &acl, _prdct_t aprd):slfclmn(acl){
		msCmp = gsEqn[aprd];
	}

	virtual void set_vl_to(std::string &vl)const{}
public:
	in_cndt(const in_cndt *a1st, _tp atp, const in_cndt *a2nd):slfclmn(_stub_clmn){
		m_tp = atp;
		m_rl[0] = a1st;
		m_rl[1] = a2nd;
	}
	inline in_cndt& create(_tp atp, const condition &acnd)const{
		auto &vcnd = dynamic_cast<const in_cndt&>(acnd);
		m_crtd = std::make_shared<in_cndt>(&vcnd, atp, this);
		return *m_crtd;
	}
	const condition& operator &&(const condition &acnd)const{
		return create(_and, acnd);
	}
	const condition& operator ||(const condition &acnd)const{
		return create(_or, acnd);
	}
	virtual std::string sql()const{
		std::string rslt;

		if (_prdct == m_tp){
			rslt = slfclmn.get_str(true);
			rslt += msCmp;
			this->set_vl_to(rslt);
			return rslt;
		}
		rslt = "(";
		rslt += m_rl[0]->sql();
		rslt += ") ";
		rslt += ((_and == m_tp)? "AND": "OR");
		rslt += " (";
		rslt += m_rl[1]->sql();
		rslt += ')';
		return rslt;
	}
	virtual ~in_cndt(){}
};
//------------------------------------------------------------------------------
request::table::column& request::table::column::any(){return _stub_clmn;}
//------------------------------------------------------------------------------
class in_rqst: public request{
	class tbl: public request::table{
	public:
		const char *m_nm;

		class clmn: public pre_rq_clmn{
			ITS_NO_STUB
		public:
			const tbl &m_tbl;
			std::string get_str(bool wthtbl) const{
				std::string rslt = "\"";

				if (wthtbl){
					rslt += m_tbl.m_nm;
					rslt += "\".";
				}
				if (m_nm == "*")
					rslt += '*';
				else{
					if (wthtbl)
						rslt += '\"';
					rslt += m_nm;
					rslt += '\"';
				}
				return rslt;
			}
			bool eq_by_nm (const clmn& othr) const{
				return (m_nm + m_tbl.m_nm) == (othr.m_nm + othr.m_tbl.m_nm);
			}
			bool operator < (const pre_rq_clmn& othr) const{

				if (othr.isStub())
					return false;
				return (m_nm + m_tbl.m_nm)
					< (othr.m_nm + dynamic_cast<const clmn&>(othr).m_tbl.m_nm);
			}

			class eq_clm: public in_cndt{
			public:
				using map_by_cmp = std::map<_prdct_t, eq_clm>;

				const clmn &m_arg;

				eq_clm(const clmn &slf, _prdct_t aPr, const clmn &oth)
												:in_cndt(slf, aPr), m_arg(oth){}
				void set_vl_to(std::string &vl)const{vl += m_arg.get_str(true);}
			};
			mutable std::map<clmn::ref, eq_clm::map_by_cmp> clmn_eq;

			const eq_clm& get_clmn_cnd(const table::column &acl, _prdct_t aPrdct)const{
				auto &rcl = dynamic_cast<const clmn&>(acl);

				eq_clm::map_by_cmp *pInMp = nullptr;

				try {
					pInMp = &(clmn_eq.at(rcl));
				}catch(std::out_of_range&){}

				if (nullptr != pInMp)
					try{return pInMp->at(aPrdct);}catch(std::out_of_range&){}
				else{
					auto vr = clmn_eq.insert({rcl, eq_clm::map_by_cmp()});

					if (!vr.second)
						throw std::runtime_error("Create column equality");

					pInMp = &(vr.first->second);
				}
				auto rr = pInMp->insert({aPrdct, eq_clm(*this, aPrdct, rcl)});

				if (!rr.second)
					throw std::runtime_error("Create column equality");

				return rr.first->second;
			}

			class eq_str: public in_cndt{
			public:
				using map_by_cmp = std::map<_prdct_t, eq_str>;

				const char *m_arg;

				eq_str(const clmn &slf, _prdct_t aPr, const char *arg)
												:in_cndt(slf, aPr), m_arg(arg){}
				void set_vl_to(std::string &vl)const{
					vl += '\'';
					vl += m_arg;
					vl += '\'';
				}
			};
			mutable std::map<std::string, eq_str::map_by_cmp> str_eq;

			const eq_str& get_str_cnd(const char *vl, _prdct_t aPrdct)const{

				eq_str::map_by_cmp *pInMp = nullptr;

				try {
					pInMp = &(str_eq.at(vl));
				}catch(std::out_of_range&){}

				if (nullptr != pInMp)
					try{return pInMp->at(aPrdct);}catch(std::out_of_range&){}
				else{
					auto vr = str_eq.insert({vl, eq_str::map_by_cmp()});

					if (!vr.second)
						throw std::runtime_error("Create string equality");

					pInMp = &(vr.first->second);
				}
				auto rr = pInMp->insert({aPrdct, eq_str(*this, aPrdct, vl)});

				if (!rr.second)
					throw std::runtime_error("Create string equality");

				return rr.first->second;
			}

			class like_str: public in_cndt{
			public:
				std::string m_str;
				like_str(const char *astr, const clmn &slf):in_cndt(slf, _unequal),m_str(astr){}
				std::string sql()const{
					std::string rslt = slfclmn.get_str(true);
					rslt += " LIKE \'%";
					rslt += m_str;
					rslt += "%\'";
					return rslt;
				}
			};
			mutable std::map<std::string,	like_str>	str_like;

			class eq_int: public in_cndt{
			public:
				using map_by_cmp = std::map<_prdct_t, eq_int>;

				int m_arg;

				eq_int(const clmn &slf, _prdct_t apr, int vl)
												:in_cndt(slf, apr), m_arg(vl){}
				void set_vl_to(std::string &vl)const{vl += std::to_string(m_arg);}
			};
			mutable std::map<int, eq_int::map_by_cmp>/*std::map<eq_int::key,	eq_int::pointer>*/	int_eq;

			const eq_int& get_int_cnd(int vl, _prdct_t aPrdct)const{

				eq_int::map_by_cmp *pInMp = nullptr;

				try {
					pInMp = &(int_eq.at(vl));
				}catch(std::out_of_range&){}

				if (nullptr != pInMp)
					try{return pInMp->at(aPrdct);}catch(std::out_of_range&){}
				else{
					auto vr = int_eq.insert({vl, eq_int::map_by_cmp()});

					if (!vr.second)
						throw std::runtime_error("Create string equality");

					pInMp = &(vr.first->second);
				}
				auto rr = pInMp->insert({aPrdct, eq_int(*this, aPrdct, vl)});

				if (!rr.second)
					throw std::runtime_error("Create string equality");

				return rr.first->second;
			}

			class eq_bool: public in_cndt{
			public:
				bool m_v;

				eq_bool(bool vl, const clmn &slf):in_cndt(slf, _equal), m_v(vl){}

				void set_vl_to(std::string &vl)const{vl += (m_v? "TRUE": "FALSE");}
			};

			mutable std::shared_ptr<eq_bool>			p_bool_eq[2];

			clmn(const char *anm, const tbl &atbl):m_tbl(atbl){m_nm = anm;}

			const condition& operator == (const table::column &acl) const{
				return get_clmn_cnd(acl, _equal);
			}
			const condition& operator != (const table::column &acl) const{
				return get_clmn_cnd(acl, _unequal);
			}
			const condition& operator == (const char *arg) const{
				return get_str_cnd(arg, _equal);
			}
			const condition& operator != (const char *arg) const{
				return get_str_cnd(arg, _unequal);
			}
			const condition& operator <= (const char *arg) const{
				return get_str_cnd(arg, _eqless);
			}
			const condition& operator >= (const char *arg) const{
				return get_str_cnd(arg, _eqmore);
			}
			const condition& roughly(const char *arg) const{
				try{return str_like.at(arg);}catch(std::exception&){}

				auto rslt = str_like.insert({arg, like_str(arg, *this)});

				if (!rslt.second)
					throw std::runtime_error("Create cl-str equality");

				return rslt.first->second;
			}
			const condition& operator == (int arg) const{
				return get_int_cnd(arg, _equal);
			}
			const condition& operator != (int arg) const{
				return get_int_cnd(arg, _unequal);
			}
			const condition& operator == (bool arg) const{
				return *(p_bool_eq[arg] = std::make_shared<eq_bool>(arg, *this));
			}
		};

		mutable std::map<std::string, clmn> clmn_map;

		const clmn& get_clmn(const char *column) const{
			if (nullptr == column || !*column)
				throw std::invalid_argument("Column name");

			try {return clmn_map.at(column);}catch(std::exception &){}

			auto rslt = clmn_map.insert({column, clmn(column, *this)});

			if (!rslt.second)
				throw std::runtime_error("Can get column");

			return rslt.first->second;
		}

		const column& operator [] (const char *column) const{
			return get_clmn(column);
		}

		class in_rq_row: public table::row{
			typedef union {int vi; const char *vs;} vv_t;
			bool mi_chng_cmplt = false;

			class in_rw_vl: public table::row::value{
				bool m_iTp_Def = false;
				selection::row::value::_type m_tp = selection::row::value::_bool;
				std::string mStr;
				int mBin = 0;
			public:
				void operator = (const char *val){
					if (nullptr == val || !*val)
						return;
					if (m_iTp_Def && (selection::row::value::_string != m_tp))
						throw std::logic_error("Change value type");
					mStr = '\'';
					mStr += val;
					mStr += '\'';
					m_tp = selection::row::value::_string;
				}
				void operator = (int vl){
					if (m_iTp_Def && (selection::row::value::_integer != m_tp))
						throw std::logic_error("Change value type");
					mBin = vl;
					m_tp = selection::row::value::_integer;
					mStr = std::to_string(vl);
				}
				void operator = (bool vl){
					if (m_iTp_Def && (selection::row::value::_bool != m_tp))
						throw std::logic_error("Change value type");
					mBin = vl;
					m_tp = selection::row::value::_bool;
					mStr = vl? "true": "false";
				}
				const std::string& str_val(){return mStr;}
			};
			const tbl &m_tbl;
			std::map<clmn::ref, in_rw_vl> m_vls;

		public:
			in_rq_row(tbl &atbl):m_tbl(atbl){}

			value& operator [] (const char *acl){

				auto &rc = m_tbl.get_clmn(acl);
				try{
					return m_vls.at(rc);
				}catch(std::exception&){}

				auto rslt = m_vls.insert({rc, in_rw_vl()});

				if (!rslt.second)
					throw std::runtime_error("Can't add value");

				return rslt.first->second;
			}
			std::string prepare_addition(){
				std::stringstream ss;
				bool n1st = false;
				ss << "INSERT INTO " << m_tbl.m_nm << " (";
				for (auto &rcl: m_vls){
					if (n1st)
						ss << ',';
					n1st = true;
					ss <<'\"' << rcl.first.mrc.m_nm << "\"";
				}
				ss << ") VALUES (";
				n1st = false;
				for (auto &rcl: m_vls){
					if (n1st)
						ss << ',';
					n1st = true;

					ss << rcl.second.str_val();
				}
				ss << ')';
				mi_chng_cmplt = true;
				return ss.str();
			}
			std::string prepare_update(){
				std::stringstream ss;
				bool n1st = false;
				ss << "UPDATE " << m_tbl.m_nm << " SET ";

				for (auto &rcl: m_vls){
					if (n1st)
						ss << ',';
					n1st = true;
					ss <<'\"' << rcl.first.mrc.m_nm << "\"=" << rcl.second.str_val();
				}
				mi_chng_cmplt = true;
				return ss.str();
			}
			bool is_complete()const{return mi_chng_cmplt;}
		};

		std::shared_ptr<in_rq_row> mp_chng_row;
		std::string m_rw_subcmd;

		tbl(const char *anm):m_nm(anm){}
		row& prepare_row(){

			if (!mp_chng_row)
				mp_chng_row = std::make_shared<in_rq_row>(*this);

			if (mp_chng_row->is_complete())
				throw std::logic_error("This request change row prepare complete");

			return *mp_chng_row;
		}
		void add(row &arw){
			auto &rRw = dynamic_cast<in_rq_row&>(arw);
			if (rRw.is_complete())
				throw std::logic_error("This request change row prepare complete");
			m_rw_subcmd = rRw.prepare_addition();
		}
		void update(row &arw){
			auto &rRw = dynamic_cast<in_rq_row&>(arw);
			if (rRw.is_complete())
				throw std::logic_error("This request change row prepare complete");
			m_rw_subcmd = rRw.prepare_update();
		}
		bool i_has_row_cmd()const{return mp_chng_row?mp_chng_row->is_complete():false;}
	};

public:
	class unn:public request::Union{
		enum {lim_cnt, lim_offs, srt_num};
		std::list<const in_rqst*> m_rqs;
		struct{
			const char *sql;
			bool iDef;
			size_t val;
		}m_lm[4] = {
			{" LIMIT ", false},{" OFFSET ", false},{" ORDER BY ", false}, {nullptr}
		};

		request::Union& operator += (const request &aRq){
			auto *prq = dynamic_cast<const in_rqst*>(&aRq);

			for (auto rrq: m_rqs){
				if (rrq == prq)
					return *this;//this request has in this union already
			}

			m_rqs.push_back(prq);
			return *this;
		}
		void limited(size_t aCount, size_t aOffs){
			m_lm[lim_cnt].iDef = true;
			m_lm[lim_cnt].val = aCount;
			m_lm[lim_offs].iDef = (0 != aOffs);
			m_lm[lim_offs].val = aOffs;
		}
		void sort_by(size_t cln);
	public:
		unn(const in_rqst &a1st, const in_rqst &a2nd){
			m_rqs.push_back(&a1st);
			m_rqs.push_back(&a2nd);
		}
		std::string get_str_rq()const{
			std::string rslt;
			bool n1st = false;

			for (auto rrq: m_rqs){
				if (n1st)
					rslt += " UNION ";
				n1st = true;
				rslt += rrq->get_str_rq();
			}

			for (auto *pCnd = m_lm; nullptr != pCnd->sql; ++pCnd){

				if (!(pCnd->iDef))
					continue;

				rslt += pCnd->sql;
				rslt += std::to_string(pCnd->val);
			}
			return rslt;
		}
	};
private:
	std::map<std::string, tbl> tbl_map;
	std::map<pre_rq_clmn::ref, const char *> m_out_cl_map, m_out_fnc;
	std::string m_fldr, m_lmts, m_srt;
	const in_cndt *mp_cnd = nullptr;
	mutable std::list<unn> mUnions;
public:
	in_rqst(const char *afldr){
		if (nullptr != afldr && *afldr)
			m_fldr = afldr;
	}
	table& use_table(const char *name){
		if (nullptr == name || !*name)
			throw std::invalid_argument("Table name");

		try {return tbl_map.at(name);} catch (std::exception &){}

		auto rslt = tbl_map.insert({name, tbl(name)});

		if (!rslt.second)
			throw std::runtime_error("Can use table");

		return rslt.first->second;
	}
	void operator = (const condition &acnd){
		mp_cnd = dynamic_cast<const in_cndt*>(&acnd);
	}
	void operator &= (const condition &acnd){
		if (nullptr == mp_cnd)
			mp_cnd = dynamic_cast<const in_cndt*>(&acnd);
		else
			mp_cnd = dynamic_cast<const in_cndt*>(&((*mp_cnd) && acnd));
	}
	void operator |= (const condition &acnd){
		if (nullptr == mp_cnd)
			mp_cnd = dynamic_cast<const in_cndt*>(&acnd);
		else
			mp_cnd = dynamic_cast<const in_cndt*>(&((*mp_cnd) || acnd));
	}
	request::Union& operator + (const request &othr)const{
		mUnions.push_back(unn(*this, dynamic_cast<const in_rqst&>(othr)));
		return mUnions.back();
	}
	void need_out(const table::column &cl, const char *alias){
		m_out_cl_map[dynamic_cast<const pre_rq_clmn &>(cl)] = alias;
	}
	void limited(size_t aCount, size_t aOffs = 0){
		std::stringstream ss;

		ss << " LIMIT " << aCount;

		if (0 != aOffs)
			ss << " OFFSET " << aOffs;

		m_lmts = ss.str();
	}
	void order_by(const table::column &acl){
		if (m_srt.empty())
			m_srt = " ORDER BY ";
		else
			m_srt += ", ";

		m_srt += dynamic_cast<const tbl::clmn&>(acl).get_str(true);
	}
	void need_out_fnc(const char *fnc, const table::column &cl){
		if (nullptr == fnc || !*fnc)
			throw std::invalid_argument("db.function name");
		m_out_fnc[dynamic_cast<const pre_rq_clmn &>(cl)] = fnc;
	}
	size_t get_cl_out_cnt()const{return m_out_cl_map.size();}
	std::string get_str_rq()const{
		std::stringstream ss;
		bool in1st = false;

		auto &r1st_tbl = tbl_map.begin()->second;

		if (r1st_tbl.i_has_row_cmd())
			ss << r1st_tbl.m_rw_subcmd;
		else{
			ss << "SELECT ";

			if (m_out_cl_map.empty() && m_out_fnc.empty())
				ss << " *";
			else{

				for (auto &rcr: m_out_cl_map){
					if (in1st)
						ss << ", ";
					in1st = true;

					ss << rcr.first.mrc.get_str(true);

					if (nullptr == rcr.second)
						continue;

					ss << " AS \"" << rcr.second << '\"';
				}
				for (auto &rcr: m_out_fnc){
					if (in1st)
						ss << ", ";
					in1st = true;

					ss << rcr.second << '(' << rcr.first.mrc.get_str(false) << ')';
				}
			}
			in1st = false;
			ss << " FROM ";

			for (auto &rtr: tbl_map){
				if (in1st)
					ss << ", ";
				in1st = true;

				if (!m_fldr.empty())
					ss << '\"' << m_fldr << "\".\"" << rtr.second.m_nm << "\" ";

				ss << '\"' << rtr.second.m_nm << '\"';
			}
		}

		if (nullptr == mp_cnd){
			ss << m_srt << m_lmts;
			return ss.str();
		}

		ss << " WHERE " << mp_cnd->sql() << m_srt << m_lmts;
		return ss.str();
	}
};
void in_rqst::unn::sort_by(size_t cln){
	if (cln < 1)
		throw std::logic_error("DB request union sort");

	size_t vMx = 0;

	for (auto &rrq: m_rqs){
		auto vCnt = rrq->get_cl_out_cnt();

		if (vMx < vCnt)
			vMx = vCnt;
	}
	if (cln > vMx)
		throw std::logic_error("DB request union sort index out of range");

	m_lm[srt_num].iDef = true;
	m_lm[srt_num].val = cln;
}
//==============================================================================
class in_value: public selection::row::value{
	std::string m_svl;
	union {
		short		i2;
		int			i4;
		long int	i8;
		float		f4;
		double		f8;
	};
	int m_dtp;
	bool m_iFl;
public:
	in_value(const char *dt, size_t sz, bool iBin, bool iFlt){
		m_iFl = iFlt;

		if (!iBin){
			m_dtp = 0;
			m_svl = dt;
			return;
		}
		if (sz > 8)
			sz = 8;

		memcpy(&i8, dt, sz);//WARN: LE only

		for (m_dtp = -1; sz; ++m_dtp, sz >>= 1);

		switch(m_dtp){
			case 1: m_svl = std::to_string(i2);	break;
			case 2: m_svl = iFlt? std::to_string(f4): std::to_string(i4); break;
			case 3: m_svl = iFlt? std::to_string(f8): std::to_string(i8); break;
		}
	}
	operator int () const{
		switch(m_dtp){
			case 0: return std::atoi(m_svl.c_str());
			case 1: return i2;
			case 2: return (!m_iFl)? i4: f4;
			case 3: return (!m_iFl)? i8: f8;
		}
		return 0;
	}
	operator double () const{

		switch(m_dtp){
			case 0: return std::atof(m_svl.c_str());
			case 2: return m_iFl? f4: i4;
			case 3: return m_iFl? f8: i8;
		}

		throw std::logic_error("mismatch type");
	}
	operator std::string () const {return m_svl;}
	operator bool () const {return m_svl == "t";}
};

class in_slct_rw: public selection::row{
	PGresult *m_rslt;
	int m_num;
	mutable std::map<int, in_value> m_values;
public:
	in_slct_rw(PGresult *arslt, int rw_num): m_rslt(arslt), m_num(rw_num){}
	const value& operator [] (const char *column)const{
		std::string vs = "\"";
		vs += column;
		vs += '\"';
		int cn = PQfnumber(m_rslt, vs.c_str());

		if (cn < 0)
			throw std::logic_error("Bad column name");

		try{
			return m_values.at(cn);
		}catch(std::exception &){}

		auto vtp = PQftype(m_rslt, cn);

		auto rslt = m_values.insert(
				{cn, in_value(PQgetvalue(m_rslt, m_num, cn), PQfsize(m_rslt, cn),
				PQfformat(m_rslt, cn), (700 == vtp) || (701 == vtp))});

		if (!rslt.second)
			throw std::runtime_error("Can't create value");

		return rslt.first->second;
	}
};

class pre_select{
protected:
	PGresult *m_rslt;
public:
	pre_select(PGresult *arslt): m_rslt(arslt){}
	PGresult *get(){return m_rslt;}
	ExecStatusType get_status(){return PQresultStatus(m_rslt);}
	~pre_select(){PQclear(m_rslt);}
};

class in_slctn: public selection, public pre_select{
	PGconn *m_conn;
	size_t m_rw_cnt;
	/*void exc_cmd(const char *scmd)const{
		pre_select cmd(PQexec(m_conn, scmd));

		if (PGRES_COMMAND_OK != cmd.get_status())
			throw std::runtime_error(PQerrorMessage(m_conn));
	}*/
public:
	in_slctn(PGresult *vrslt, PGconn *acnn):pre_select(vrslt), m_conn(acnn){
		m_rw_cnt = PQntuples(m_rslt);
	}
	row::pointer operator [] (size_t ix) const{
		if (m_rw_cnt <= ix)
			throw std::range_error("Index row in selection");
		return std::make_shared<in_slct_rw>(m_rslt, ix);
	}
	void IterateBy(yxa::Cursor<row> &rwcr, void *pud) const{

		for (size_t i= 0; i < m_rw_cnt; ++i){
			in_slct_rw rw(m_rslt, i);

			if (!rwcr.continue_after(rw, pud, i, (m_rw_cnt - 1) == i))
				return;
		}
	}
	size_t get_row_count() const {return m_rw_cnt;}
};

class in_cnnctn: public connection{
	PGconn *m_cnn;
	std::mutex m_mx_exc;

	selection::pointer in_select(const char *srq){
		std::lock_guard<std::mutex> keep(m_mx_exc);
		auto ppgr = PQexec(m_cnn, srq);

		if (nullptr == ppgr)
			return nullptr;

		ExecStatusType pqrs;

		for (;;){
			pqrs = PQresultStatus(ppgr);

			if ((PGRES_TUPLES_OK == pqrs) || (PGRES_SINGLE_TUPLE == pqrs))
				break;

			if ((PGRES_EMPTY_QUERY		== pqrs) //nonsense
				|| (PGRES_COMMAND_OK	== pqrs)//empty response
				|| (PGRES_BAD_RESPONSE	== pqrs)
				|| (PGRES_NONFATAL_ERROR== pqrs)
				|| (PGRES_FATAL_ERROR	== pqrs))
				return nullptr;
		}

		return std::make_shared<in_slctn>(ppgr, m_cnn);

	}
public:
	in_cnnctn(PGconn *acnn):m_cnn(acnn){}
	selection::pointer select(const char *table, std::list<const char*> columns){
		std::stringstream ss;
		ss << "SELECT ";

		if (columns.empty())
			ss << "* ";
		else{
			bool in1st = false;

			for (auto *csc: columns){
				if (in1st)
					ss << ", ";
				in1st = true;
				ss << '\"' << csc << '\"';
			}
			ss << ' ';
		}
		ss << "FROM " << table << ';';

//		std::cout << '[' << ss.str() << ']' << std::endl;
		return in_select(ss.str().c_str());
	}
	request::pointer create_request(const char *db_foldr){
		return std::make_shared<in_rqst>(db_foldr);
	}
	selection::pointer select(const request &rq){
		auto srq = dynamic_cast<const in_rqst&>(rq).get_str_rq();
		srq += ';';
//		std::cout << '[' << srq << ']' << std::endl;
		return in_select(srq.c_str());
	}
	selection::pointer select(const request::Union &aun){
		std::string vRqStr = dynamic_cast<const in_rqst::unn&>(aun).get_str_rq();
		vRqStr += ';';
		return in_select(vRqStr.c_str());
	}
	~in_cnnctn(){PQfinish(m_cnn);}
};
//------------------------------------------------------------------------------
connection::pointer connection::open(const char *db_name,
										const char *login, const char *passw,
										const char *host, unsigned short port){

	if (nullptr == db_name || !*db_name)
		throw std::invalid_argument("db_name");

	std::stringstream ss;

	if (nullptr != host)
		ss << "host=" << host << ' ' << "port=" << port << ' ';

	ss << "dbname=" << db_name;

	if (nullptr != login && *login){
		ss << " user=" << login;

		if (nullptr != passw && *passw)
			ss << " password=" << passw;
	}

	PGconn *pCnn = PQconnectdb(ss.str().c_str());
	ConnStatusType vCST;

	do {
		vCST = PQstatus(pCnn);

		if (CONNECTION_BAD == vCST)
			return nullptr;

	}while (CONNECTION_OK != vCST);

	return std::make_shared<in_cnnctn>(pCnn);
}
//==============================================================================
EMPTY_DESTRUCTOR(request)
request::EMPTY_DESTRUCTOR(condition)
request::EMPTY_DESTRUCTOR(table)
request::table::EMPTY_DESTRUCTOR(column)
request::table::row::EMPTY_DESTRUCTOR(value)
request::table::EMPTY_DESTRUCTOR(row)
request::EMPTY_DESTRUCTOR(Union)
selection::row::EMPTY_DESTRUCTOR(value)
selection::EMPTY_DESTRUCTOR(row)
EMPTY_DESTRUCTOR(selection)
EMPTY_DESTRUCTOR(connection)
//------------------------------------------------------------------------------
void yxa_db_compile_test(){
	auto pdbc = connection::open("dbname", "login", "passw");
	auto prq = pdbc->create_request();

	auto &usr_tbl = prq->use_table("v_users");
	auto &acc_tbl = prq->use_table("v_access");

	*prq =	(usr_tbl["ID"] == acc_tbl["ID"]) &&
			(usr_tbl["Name"] == "login") &&
			(acc_tbl["Password"] == "passw");

	prq->need_out(usr_tbl["ID"], "UID");
	prq->need_out(acc_tbl["Role"]);

	auto pslct = pdbc->select(*prq);
}
}}
