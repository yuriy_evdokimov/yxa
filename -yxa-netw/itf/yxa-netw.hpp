/*	Y-eXtensible Architecture
 *
 * network.hpp
 *
 *  Created on: 22 mar 2016.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */

#pragma once

#include <vector>
#include <exception>
#include <netinet/in.h>
#include <yxa-itr-def.h>

namespace yxa::net{

INTERFACE(interface)

	CONST_METHOD(const std::string& Name())
	CONST_METHOD(bool isUP())
	CONST_METHOD(bool isLoop())
	CONST_METHOD(bool isPPP())
	CONST_METHOD(bool isNoIP())
	CONST_METHOD(const std::string& MAC())

	INTERFACE(ip)
		WITH_ENUM(prop, prp_self, prp_mask, prp_broadcast, prp_subnet)

		CONST_METHOD(interface& binded())
		CONST_METHOD(in_addr_t nt_addr(prop aprp = prp_self))
		CONST_METHOD(std::string str_addr(prop))
		HAVE_STUB(any)
		CAN_USE_BY_POINTER
	INTRF_END

	OPERATOR(const ip&, [], size_t, const)
	IT_CONTAINS_CNT(ip)

	INTERFACE_SH(browse, snapshot())
		CONST_METHOD(size_t get_interfaces_count())
		CONST_METHOD(size_t get_subnets_count())

		IT_CONTAINS(interface)
		IT_CONTAINS(ip::pointer)
	INTRF_END
INTRF_END
//------------------------------------------------------------------------------
INTERFACE(sock)
	WITH_ENUM(_type, UDP_broadcast, TCP_client, TCP_server)
	WITH_TYPE(data, std::vector<unsigned char>)

	CAN_USE_BY_POINTER
	HAS_EQUAL_OPERATOR
	OPERATOR(bool, <, const sock&, const)//for mapping

	CONST_METHOD(bool isType(_type))
	SELF_OPERATOR(<<, const data&)//send data
	//sync receive,
	//interrupted by selector::manager::isCanContinue(),
	//use only with receiver::none(), else throw exception
	SELF_OPERATOR(>>, data&)

	REFERENCE//for mapping by reference
		SUPPORT_OPERATOR(bool, <, const)
	REF_END

	CAN_BE_DEFINE(user_handling_obj)
	INTRF_END

	METHOD(void attach(user_handling_obj&))
	CONST_METHOD(user_handling_obj& attached())

	CAN_BE_DEFINE(receiver)
		WITH_METHOD(bool handle_rcv(sock&, data&, in_addr_t ip, in_port_t))
		HAVE_STUB(none)
	INTRF_END

	CAN_BE_DEFINE(except_handler)
		WITH_ENUM(exc_cd, exc_rcv, exc_link_fail, exc_discnnect)
		WITH_METHOD(void handle_exc(sock&, exc_cd))
		HAVE_STUB(none)
	INTRF_END

	//forward, define above
	class selector;

	CAN_BE_DEFINE(server)

		CAN_BE_DEFINE(acceptor)
			MAY_REDEFINE_METHOD(receiver&	receive_async_by())
			MAY_REDEFINE_METHOD(selector&	custom_clients_selector())
			HAVE_STUB(none)
		INTRF_END

		METHOD(acceptor& accept_by(in_addr_t remoteIP))//for decline need return acceptor::none()
		METHOD(void on_accepted(in_addr_t remoteIP, in_port_t remoteP, sock::pointer&))
	INTRF_END

	INTERFACE_SH(selector, create())
		METHOD(sock::pointer add_UDP_broadcast(in_port_t,
						receiver			&rc = receiver::none(),
						except_handler		&eh = except_handler::none(),
						const interface::ip	&bnd = interface::ip::any()))
		METHOD(sock::pointer add_TCP_client(in_addr_t, in_port_t,
						receiver			&rc = receiver::none(),
						except_handler		&eh = except_handler::none(),
						const interface::ip	&bnd = interface::ip::any()))
		METHOD(sock::pointer add_TCP_server(in_port_t, size_t max_connection,
						server				&srv,
						except_handler		&eh = except_handler::none(),
						const interface::ip	&allow_accept = interface::ip::any()))
		METHOD(void remove(sock&))//sock not handled after, but not destroy, while shared pointer not expired

		CAN_BE_DEFINE(manager)
			WITH_ENUM(wait, ms_select, ms_no_sck)
			WITH_METHOD(bool isCanContinue())
			MAY_REDEFINE_METHOD(size_t get_period_ms(wait))
		INTRF_END

		METHOD(void process(manager&))//interrupt by manager::isCanContinue()
		HAVE_STUB(none)//for sync receiving from accepted server clients
	INTRF_END
INTRF_END
//==============================================================================
}//yxa::net namespace
