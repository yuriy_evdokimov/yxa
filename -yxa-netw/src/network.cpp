/*	Y-eXtensible Architecture
 *
 * network.cpp
 *
 *  Created on: 22 mar 2016.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */
#include <map>
#include <list>
#include <cstring>
#include <mutex>
//#include <iostream>
#include <unistd.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <yxa-netw.hpp>

namespace yxa::net{

typedef std::lock_guard<std::mutex> lck_keeper;
class sck_rlsn;//forward
//------------------------------------------------------------------------------
class proto_slctr{
public:
	virtual void inr_add_sock(std::shared_ptr<sck_rlsn>&) = 0;
	virtual ~proto_slctr(){}
	static proto_slctr& stub(){
		static class :public proto_slctr {
			void inr_add_sock(std::shared_ptr<sck_rlsn>&){}
		}_stub_;
		return _stub_;
	}
};
static class :public sock::user_handling_obj{} uho_stub_;
//------------------------------------------------------------------------------
class sck_rlsn: public sock{
protected:
	typedef std::pair<in_addr_t, in_port_t> net_point_t;

	int m_fd;
	in_addr_t m_addr = htonl(INADDR_BROADCAST);
	in_port_t m_rmt_prt;
	sock::receiver			&mrr;
	sock::except_handler	&mex;
	user_handling_obj		*muho = &uho_stub_;

	virtual bool is_sync_recv_cmplt(data &drcv, net_point_t &anp, proto_slctr &) = 0;
public:
	typedef std::shared_ptr<sck_rlsn> ptr;

	sck_rlsn(int afd, in_port_t aprt, sock::receiver &arr, sock::except_handler &aex):
		m_fd(afd), m_rmt_prt(aprt), mrr(arr), mex(aex){}
	virtual bool can_use_ip(in_addr_t){return true;}
	int get_fd() const {return m_fd;}
	bool operator == (const sock &othr) const{
		return m_fd == ((sck_rlsn*)&othr)->m_fd;
	}
	bool operator < (const sock &othr) const{
		return m_fd < ((sck_rlsn*)&othr)->m_fd;
	}
	void attach(user_handling_obj &auho){muho = &auho;}
	user_handling_obj& attached()const {return *muho;}

	virtual long rcv_bytes_ready(){
		long rslt = 0;

		if (0 > ioctl(m_fd, FIONREAD, &rslt))
			return -1;

		return rslt;
	}
	bool async_receive(proto_slctr &slr){
		net_point_t vnp;

		auto lb = this->rcv_bytes_ready();

		if (lb <= 0){
			mex.handle_exc(*this, except_handler::exc_discnnect);
			return false;
		}

		data vdt(lb, 0);

		if (!this->is_sync_recv_cmplt(vdt, vnp, slr))
			return false;

		try {
			return mrr.handle_rcv(*this, vdt, vnp.first, vnp.second);
		}catch (std::exception&){}//troubles the external users...
		return true;
	}
	sock& operator >> (sock::data &dt){
		enum {def_inc_pck_sz = 512};
		net_point_t vnp;

		if (!mrr.isStub())
			throw std::logic_error("Try sync receive with async socket receiving");

		if (dt.size() < def_inc_pck_sz)
			dt.resize(def_inc_pck_sz);

		static std::mutex snd_lck;
		lck_keeper keep(snd_lck);
		this->is_sync_recv_cmplt(dt, vnp, proto_slctr::stub());
		return *this;
	}
	void handle_exception(except_handler::exc_cd err_cd){
		mex.handle_exc(*this, err_cd);
	}
	virtual ~sck_rlsn(){
		shutdown(m_fd, SHUT_RDWR);
		close(m_fd);
	}
};
//------------------------------------------------------------------------------
class udp_br_sck: public sck_rlsn{
	bool is_sync_recv_cmplt(data &drcv, net_point_t &anp, proto_slctr &){
		struct sockaddr_in vfrom;
		unsigned int vsz = sizeof(vfrom);

		ssize_t vrb = recvfrom(m_fd, drcv.data(), drcv.size(), 0,
				(struct sockaddr*)&vfrom, &vsz);

		if (0 == vrb)//Nonsense
			return false;

		if (0 > vrb){
			try {mex.handle_exc(*this, except_handler::exc_rcv);} catch(...){}
			return false;
		}
		if (vrb != (ssize_t)drcv.size())
			drcv.resize(vrb);

		anp.first = vfrom.sin_addr.s_addr;
		anp.second = vfrom.sin_port;
		return true;
	}
public:
	udp_br_sck(int afd, in_port_t aprt, sock::receiver &arr,
					sock::except_handler &aex):sck_rlsn(afd, aprt, arr, aex){}
	bool can_use_ip(in_addr_t aaddr){
		m_addr = aaddr;
		return true;
	}
	bool isType(_type atp) const {return sock::UDP_broadcast == atp;}
	sock& operator <<(const sock::data &dt){
		/*struct msghdr vmh;
		struct iovec vio;
		struct in_pktinfo vpi;*/
		struct sockaddr_in vssa;
		int yes = 1;

		if (dt.empty())
			throw std::logic_error("Empty data for sending");

		if (-1 == setsockopt(m_fd, SOL_SOCKET, SO_BROADCAST, (char*)&yes, sizeof(yes)))
			throw std::runtime_error("Can't set UDP-bcst socket option");

		memset(&vssa, 0, sizeof(vssa));
		vssa.sin_family = AF_INET;
		vssa.sin_addr.s_addr = m_addr;
		vssa.sin_port = htons(m_rmt_prt);

		/*vio.iov_base = (void*)dt.data();
		vio.iov_len = dt.size();

		vpi.ipi_ifindex = 0;
		vpi.ipi_addr.s_addr = 0xC0A8012F;
		vpi.ipi_spec_dst.s_addr = htonl(INADDR_BROADCAST);

		memset(&vmh, 0, sizeof(vmh));
		vmh.msg_name = &vssa;
		vmh.msg_namelen = sizeof(vssa);
		vmh.msg_iov = &vio;
		vmh.msg_iovlen = 1;
		vmh.msg_control = &vpi;
		vmh.msg_controllen = sizeof(vpi);
		std::cout << "UDP bcts send over " << m_fd << std::endl;
		sendmsg(m_fd, &vmh, 0);*/

		sendto(m_fd, dt.data(), dt.size(), 0, //result not important
				(const struct sockaddr*) &vssa, sizeof(vssa));
		return *this;
	}
	static sck_rlsn::ptr create(int afd, in_port_t aprt,
			sock::receiver	&rc, sock::except_handler &sh, sock::server &ss){
		return std::make_shared<udp_br_sck>(afd, aprt, rc, sh);
	}
};
//------------------------------------------------------------------------------
class tcp_cl_sck: public sck_rlsn{
	bool is_sync_recv_cmplt(data &drcv, net_point_t &anp, proto_slctr &){
		auto vrb = recv(m_fd, drcv.data() , drcv.size() , 0);

		if (0 >= vrb){
			try {mex.handle_exc(*this,(0 == vrb)?	except_handler::exc_discnnect:
													except_handler::exc_rcv);
			} catch(...){}
			return false;
		}
		if (vrb != (ssize_t)drcv.size())
			drcv.resize(vrb);

		anp.first = m_addr;
		anp.second = m_rmt_prt;
		return true;
	}
public:
	tcp_cl_sck(int afd, in_addr_t ip, in_port_t aprt, sock::receiver &arr,
			sock::except_handler &aex):	sck_rlsn(afd, aprt, arr, aex){
		m_addr = ip;
	}
	bool can_use_ip(in_addr_t aip){
		struct sockaddr_in vsa;

		memset(&vsa, 0, sizeof(vsa));
		vsa.sin_family = AF_INET;
		vsa.sin_addr.s_addr = aip;
		vsa.sin_port = htons(m_rmt_prt);

		if (0 != connect(m_fd, (struct sockaddr*)&vsa, sizeof(vsa)))
			return false;

		m_addr = aip;
		return true;
	}
	bool isType(_type atp) const {return sock::TCP_client == atp;}
	sock& operator <<(const sock::data &dt){

		if (dt.empty())
			throw std::logic_error("Empty data for sending");

		ssize_t sns_sz = dt.size();

		if (sns_sz > ::send(m_fd, dt.data(), sns_sz, 0))
			throw std::runtime_error("Can't send to TCP server");

		return *this;
	}
	static sck_rlsn::ptr create(int afd, in_port_t aprt,
			sock::receiver	&rc, sock::except_handler &sh, sock::server &ss){
		return std::make_shared<tcp_cl_sck>(afd, 0, aprt, rc, sh);
	}
};
//------------------------------------------------------------------------------
class tcp_srv_sck: public sck_rlsn{
	server &m_usr_srv;

	class psd_rcv: public sock::receiver {
		ITS_NO_STUB
		server &m_us;
		std::weak_ptr<sock> pclnt;
	public:
		psd_rcv(server &srv):m_us(srv){}
		void set_cl(sck_rlsn::ptr &pcl){pclnt = pcl;}
		bool handle_rcv(sock&, data&, in_addr_t ip, in_port_t prt){
			try {
				auto vpcl = pclnt.lock();
				m_us.on_accepted(ip, prt, vpcl);
			}catch(std::exception&){}
			return true;
		}
	}m_accptr;

	long rcv_bytes_ready(){return 1;}
	bool is_sync_recv_cmplt(data &drcv, net_point_t &anp, proto_slctr &slctr);
public:
	tcp_srv_sck(int afd, in_port_t aprt, sock::server &uss,
				sock::except_handler &aex):	sck_rlsn(afd, aprt, m_accptr, aex),
												m_usr_srv(uss), m_accptr(uss){}
	bool can_use_ip(in_addr_t aaddr){
		return 0 <= listen(m_fd, (int)aaddr);
	}
	bool isType(_type atp) const {return sock::TCP_server == atp;}
	sock& operator <<(const sock::data &dt){return *this;}//TODO: multicast to clients
	sock& operator >> (sock::data &dt){
		throw std::logic_error("Server can't receive without connection");
	}
	static sck_rlsn::ptr create(int afd, in_port_t aprt,
			sock::receiver	&rc, sock::except_handler &sh, sock::server &ss){
		int on = 1;

		auto rslt = std::make_shared<tcp_srv_sck>(afd, aprt, ss, sh);

		if (0 > setsockopt(afd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)))
			return nullptr;

		if (0 > ioctl(afd, FIONBIO, &on))
			return nullptr;

		return rslt;
	}
};
//==============================================================================
sock::receiver& sock::server::acceptor::receive_async_by(){
	return receiver::none();
}
sock::selector& sock::server::acceptor::custom_clients_selector(){
	return sock::selector::none();
}
//==============================================================================
sock::receiver& sock::receiver::none(){
	static class: public sock::receiver{
		ITS_STUB
		bool handle_rcv(sock&, data&, in_addr_t ip, in_port_t){
			return true;
		}
	}_stub_;
	return _stub_;
}
sock::except_handler& sock::except_handler::none(){
	static class: public sock::except_handler{
		ITS_STUB
		void handle_exc(sock&, exc_cd){}
	}_stub_;
	return _stub_;
}
sock::server::acceptor& sock::server::acceptor::none(){
	static class: public sock::server::acceptor{ITS_STUB} _stub_;
	return _stub_;
}
size_t sock::selector::manager::get_period_ms(manager::wait wt){
	return (manager::ms_select == wt)? 500: 25;
}
//==============================================================================
static class: public sock::server{
	size_t get_max_clients_connections()const{return 0;}
	acceptor& accept_by(in_addr_t remoteIP){return acceptor::none();}
	void on_accepted(in_addr_t remoteIP, in_port_t remoteP, sock::pointer&){}
}_srv_stub;
//------------------------------------------------------------------------------
class sck_slctr: public sock::selector, public proto_slctr{
	ITS_NO_STUB
	std::map<int, sck_rlsn::ptr> m_scks;
	std::mutex m_scks_lck;
public:
	void inr_add_sock(sck_rlsn::ptr &psck){
		lck_keeper keep(m_scks_lck);
		m_scks[psck->get_fd()] = psck;
	}
private:
	inline sck_rlsn::ptr ret_aftr_add(sck_rlsn::ptr psck){
		if (!psck)
			return nullptr;
		inr_add_sock(psck);
		return psck;
	}
	static sck_rlsn::ptr create_any(enum __socket_type ast, in_addr_t aip,
								sck_rlsn::ptr (*fbrq)(int,
		in_port_t,		sock::receiver&,	sock::except_handler&,		sock::server&),
		in_port_t aprt,	sock::receiver	&rc,sock::except_handler &sh,	sock::server &ss,
		const interface::ip &aiip){

		int vfd = socket(AF_INET, ast, 0);

		if (0 > vfd)
			return nullptr;


		sck_rlsn::ptr rslt = fbrq(vfd, aprt, rc, sh, ss);

		if (!rslt)
			return nullptr;

		bool itsSrv = &_srv_stub != &ss;
		bool iBindItf = !aiip.isStub();

		if (!iBindItf && !itsSrv)//no bind to net::interface for clients
			return rslt->can_use_ip(aip)? rslt: nullptr;

		if (iBindItf){

			auto &s_if = aiip.binded().Name();

			if (-1 == setsockopt(vfd, SOL_SOCKET, SO_BINDTODEVICE, s_if.c_str(),
																s_if.length()))
				return nullptr;
		}

		struct sockaddr_in vsa;
		memset(&vsa, 0, sizeof(vsa));
		vsa.sin_family = AF_INET;
		vsa.sin_addr.s_addr = iBindItf? aiip.nt_addr(interface::ip::prp_self): INADDR_ANY;
		vsa.sin_port = itsSrv? htons(aprt): 0;

		if (0 != bind(vfd, (struct sockaddr*)&vsa, sizeof(vsa)))
			return nullptr;

		return rslt->can_use_ip(aip)? rslt: nullptr;
	}
	inline size_t get_slpintvl(manager &m){//ms to usec's
		size_t rslt = m.get_period_ms(sock::selector::manager::ms_no_sck)*1000;
		return rslt?:25000;
	};
public:
	sock::pointer add_UDP_broadcast(in_port_t aprt, sock::receiver	&rc,
								sock::except_handler &sh, const interface::ip &aiip){
		return ret_aftr_add(create_any(SOCK_DGRAM,
						aiip.nt_addr(interface::ip::prp_broadcast),
						&udp_br_sck::create, aprt, rc, sh, _srv_stub, aiip));
	}
	sock::pointer add_TCP_client(in_addr_t aip, in_port_t aprt,
								sock::receiver	&rc, sock::except_handler &sh,
								const interface::ip &aiip){
		return ret_aftr_add(create_any(SOCK_STREAM, aip,
						&tcp_cl_sck::create, aprt, rc, sh,	_srv_stub, aiip));
	}
	sock::pointer add_TCP_server(in_port_t aprt, size_t mxc, sock::server &ss,
								sock::except_handler &sh, const  interface::ip &aiip){
		return ret_aftr_add(create_any(SOCK_STREAM,	mxc,
			&tcp_srv_sck::create, aprt, sock::receiver::none(), sh, ss, aiip));
	}

	void remove(sock &sck){
		lck_keeper keep(m_scks_lck);
		m_scks.erase(dynamic_cast<sck_rlsn&>(sck).get_fd());
	}
	void process(manager &intr){

		fd_set vssr, vssx;
		struct timeval vtmt = {0, 500000};
		int i, vsc = 0, mxsck = 0;
		bool iExc, iRcv;
		sck_rlsn::ptr psck;
		size_t intrvl;

		for(; intr.isCanContinue(); vsc = 0){

			{
				lck_keeper keep(m_scks_lck);

				if (m_scks.empty()){
					m_scks_lck.unlock();
					usleep(get_slpintvl(intr));
					continue;
				}
				FD_ZERO(&vssr);

				for (auto &psck: m_scks){

					if (psck.first > mxsck)
						mxsck = psck.first;

					FD_SET(psck.first, &vssr);
				}
			}
			memcpy(&vssx, &vssr, sizeof(vssr));
			if (0 == (intrvl = intr.get_period_ms(sock::selector::manager::ms_select)))
				intrvl = 500;

			vtmt.tv_sec = intrvl / 1000;
			vtmt.tv_usec = (intrvl % 1000) * 1000;

			if (0 >= (vsc = select(mxsck + 1, &vssr, NULL, &vssx, &vtmt)))
				continue;

			for (i = 0; (i <= mxsck) && vsc; ++i){

				if((iExc = FD_ISSET(i, &vssx)))
					--vsc;
				if((iRcv = FD_ISSET(i, &vssr)))
					--vsc;
				if (!iExc && !iRcv)
					continue;

				{
					lck_keeper keep(m_scks_lck);
					auto itr = m_scks.find(i);
					psck = (m_scks.end() != itr)? itr->second: nullptr;
				}
				if (!psck){
					usleep(get_slpintvl(intr));
					continue;
				}

				if (iExc)//TODO: need extend code from errno
					psck->handle_exception(sock::except_handler::exc_link_fail);
				if (iRcv && !psck->async_receive(*this)){
					lck_keeper keep(m_scks_lck);
					m_scks.erase(psck->get_fd());
				}
				psck = nullptr;
			}
		}
	}
};
sock::selector::pointer sock::selector::create(){
	return std::make_shared<sck_slctr>();
}
sock::selector& sock::selector::none(){
	static class: public sock::selector{
		ITS_STUB
		const char* msg_construct_sock = "Create socket on none selector";
	public:
		sock::pointer add_UDP_broadcast(in_port_t aprt, sock::receiver	&rc,
													sock::except_handler &sh,
													const interface::ip &aiip){
			throw std::logic_error(msg_construct_sock);
		}
		sock::pointer add_TCP_client(in_addr_t aip, in_port_t aprt,
													sock::receiver	&rc,
													sock::except_handler &sh,
													const interface::ip &aiip){
			throw std::logic_error(msg_construct_sock);
		}
		sock::pointer add_TCP_server(in_port_t, size_t, server&, except_handler&,
													const interface::ip&){
			throw std::logic_error(msg_construct_sock);
		}
		void remove(sock &sck){}
		void process(manager &intr){
			throw std::logic_error("Processing NONE socket selector");
		}
	}_stub_;
	return _stub_;
}
//==============================================================================
bool tcp_srv_sck::is_sync_recv_cmplt(data &drcv, net_point_t &anp,
															proto_slctr &slctr){
	struct sockaddr_in vsa;
	socklen_t la = sizeof(vsa);
	int vsd = accept(m_fd, (struct sockaddr*)&vsa, &la);

	if (0 > vsd){
		//TODO: need errno value analyze for not erase from select set
//		std::cout << "£+! " << __LINE__ << " (" << errno << ')' << std::endl;
		return false;
	}

	auto &macc = m_usr_srv.accept_by(vsa.sin_addr.s_addr);

	sck_rlsn::ptr clnt =
				std::make_shared<tcp_cl_sck>(vsd, vsa.sin_addr.s_addr,
									vsa.sin_port, macc.receive_async_by(), mex);
	if (macc.isStub())//by user decline accepted sock off by automatically destroying {clnt}
		return true;

	anp.first = vsa.sin_addr.s_addr;
	anp.second = vsa.sin_port;
	m_accptr.set_cl(clnt);

	auto &russl = macc.custom_clients_selector();

	if (russl.isStub())
		slctr.inr_add_sock(clnt);
	else
		dynamic_cast<sck_slctr&>(russl).inr_add_sock(clnt);

	return true;//temporary
}
//==============================================================================
EMPTY_DESTRUCTOR(sock)
sock::EMPTY_DESTRUCTOR(selector)
}
