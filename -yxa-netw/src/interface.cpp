/*	Y-eXtensible Architecture
 *
 * interface.cpp
 *
 *  Created on: 22 марта 2016 г.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
  *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */
#include <string>
#include <cstring>
#include <list>
#include <map>
#include <stdexcept>
#include <net/if.h>
#include <sys/ioctl.h>
#include <ifaddrs.h>

#include <yxa-netw.hpp>

namespace yxa::net{
//==============================================================================
class in_itf;//forward

using p_itf_t = std::shared_ptr<in_itf>;

class in_ip:public interface::ip{
	ITS_NO_STUB
	struct sockaddr maddr[3];//[0] - addr, [1] - netmask, [2] - broad
	p_itf_t mitf;
public:
	using ptr = std::shared_ptr<in_ip>;
	using pmap = std::map<in_addr_t, ptr>;
	using rmap = std::map<in_addr_t, in_ip&>;

	in_ip(struct ifaddrs *asa){
		memset(maddr, 0, sizeof(maddr));
		memcpy(maddr + 0, asa->ifa_addr,				sizeof(struct sockaddr));

		if (nullptr != asa->ifa_netmask)
			memcpy(maddr + 1, asa->ifa_netmask,			sizeof(struct sockaddr));
		if (nullptr != asa->ifa_ifu.ifu_broadaddr)
			memcpy(maddr + 2, asa->ifa_ifu.ifu_broadaddr,sizeof(struct sockaddr));
	}
	void bind_to(p_itf_t &pitf){mitf = pitf;}
	interface& binded()const;
	in_addr_t nt_addr(prop aprp)const{
		if (prp_subnet != aprp)
			return ((sockaddr_in*)&(maddr[aprp]))->sin_addr.s_addr;

		auto ns = ((sockaddr_in*)&(maddr[prp_self]))->sin_addr.s_addr;
		auto nm = ((sockaddr_in*)&(maddr[prp_mask]))->sin_addr.s_addr;

		return ns & nm;
	}
	std::string str_addr(prop aprp)const{
		std::string rslt = "";
		unsigned char vprb[4];

		if (prp_subnet == aprp){
			auto n = nt_addr(prp_subnet);
			memcpy(vprb, &n, 4);
		}else
			memcpy(vprb, maddr[aprp].sa_data + 2, 4);

		for (int j = 0; j < 4; ++j){
			rslt += std::to_string(vprb[j]);

			if (3 != j)
				rslt += '.';
		}
		return rslt;
	}
	~in_ip(){}
};
//------------------------------------------------------------------------------
class in_itf: public interface{
	std::string m_Nm, m_mac;
	int m_flgs;
	in_ip::rmap m_ips;
public:
	using ptr = std::shared_ptr<in_itf>;
	using pmap = std::map<std::string, ptr>;

	in_itf(const char *nm, const char *aMac, int flgs):m_Nm(nm), m_mac(aMac), m_flgs(flgs){}
	const std::string& Name()const{return m_Nm;}
	bool isUP()		const{return IFF_UP			& m_flgs;}
	bool isLoop()	const{return IFF_LOOPBACK	& m_flgs;}
	bool isPPP()	const{return IFF_POINTOPOINT& m_flgs;}
	bool isNoIP()	const{return m_ips.empty();};
	const std::string& MAC() const{return m_mac;}
	REALIZ_ITR_BY_STL_CNTNR (interface::ip, m_ips, .second,);
	size_t get_ip_count() const{return m_ips.size();}

	void add(in_ip &aip){
		m_ips.emplace(aip.nt_addr(ip::prp_self), aip);
	}
	const interface::ip& operator [] (size_t ix) const{

		if (ix >= m_ips.size())
			throw std::out_of_range("IP index in net-interface");

		auto it = m_ips.begin();
		std::advance(it, ix);
		return it->second;
	}
	~in_itf(){}
};
interface& in_ip::binded() const{return *mitf;}
//------------------------------------------------------------------------------
class itrf_snapsh: public interface::browse{
private:
	in_itf::pmap m_itfs;
	in_ip::pmap m_sbnts;

	REALIZ_ITR_BY_STL_CNTNR(interface, m_itfs, .second, *)
	size_t get_interfaces_count()const{return m_itfs.size();}
	size_t get_subnets_count()const{return m_sbnts.size();}
	void IterateBy(yxa::Cursor<interface::ip::pointer> &anyc, void *_ud_) const{
		auto rmn = m_sbnts.size();
		int _stp = -1;
		for (auto &pni: m_sbnts){
			interface::ip::pointer ppc = pni.second;
			if (!anyc.continue_after(ppc, _ud_, ++_stp, !--rmn))
					break;
		}
	}
public:
	itrf_snapsh(in_itf::pmap &aitfs, in_ip::pmap &aips){
		m_itfs.swap(aitfs);
		m_sbnts.swap(aips);
	}
};
//------------------------------------------------------------------------------
interface::browse::pointer interface::browse::snapshot(){
	struct ifaddrs 		*paddr;
	const char vnull[4] = {0};//for compare
	int verr = getifaddrs(&paddr);
	static const char* noMAC = "none";

	if (0 != verr)
		throw std::runtime_error(strerror(errno));

	in_itf::pmap v_itfs;
	in_ip::pmap v_sbnts;

	for (; nullptr != paddr; paddr = paddr->ifa_next){

		if ((nullptr == paddr->ifa_addr) || (nullptr == paddr->ifa_netmask) ||
				(0 == memcmp(paddr->ifa_addr->sa_data + 2, vnull, sizeof(vnull))))
			continue;

		auto vna = ((sockaddr_in*)(paddr->ifa_addr))->sin_addr.s_addr;
		auto vipr = v_sbnts.emplace(vna, std::make_shared<in_ip>(paddr));

		if (!vipr.second)
			continue;

		auto ifnm = paddr->ifa_name;
		const char *vmac = noMAC;
		char vbf[32];

		while (0 != (paddr->ifa_flags & IFF_LOOPBACK & IFF_POINTOPOINT)){

			int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);

			if (sock < 0)
				break;

			struct ifreq ifr;
			strcpy(ifr.ifr_name, ifnm);

			if (0 != ioctl(sock, SIOCGIFHWADDR, &ifr))
				break;

			char *vhw = ifr.ifr_hwaddr.sa_data;

			snprintf(vbf, 32, "%02X:%02X:%02X:%02X:%02X:%02X", (int)*vhw,
					(int)vhw[1], (int)vhw[2], (int)vhw[3], (int)vhw[4], (int)vhw[5]);
			vmac = vbf;
			break;
		}
		auto vifr = v_itfs.emplace(ifnm, std::make_shared<in_itf>(ifnm, vmac, paddr->ifa_flags));
		auto &pif = vifr.first->second;
		auto &pip = vipr.first->second;
		pip->bind_to(pif);
		pif->add(*pip);
	}
	freeifaddrs(paddr);
	return std::make_shared<itrf_snapsh>(v_itfs, v_sbnts);
}
//------------------------------------------------------------------------------
interface::ip& interface::ip::any(){
	static class: public interface::ip{
		ITS_STUB
		interface& binded()	const{throw std::logic_error("ip-stub not binded");}
		in_addr_t nt_addr(prop)	const{return 0;}
		std::string str_addr(prop)const{return "none";}
	}_stub_;
	return _stub_;
}
//==============================================================================
EMPTY_DESTRUCTOR(interface)
interface::EMPTY_DESTRUCTOR(ip)
interface::EMPTY_DESTRUCTOR(browse)
}
