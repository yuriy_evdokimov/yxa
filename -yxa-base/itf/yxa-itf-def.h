/*	Y-eXtensible Architecture
 *
 *	yxa-itf-def.h
 *
 *  Created on: 22 mar 2016.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */

#pragma once
#include <stddef.h>
#include <stdint.h>
#include <memory>

#define ITF_HDR(_CL_)class _CL_ {\
	typedef _CL_ this_type;

#define INTERFACE(_CL_) ITF_HDR(_CL_)\
	protected: virtual ~_CL_() = default;\
	public:

#define CAN_BE_DEFINE(_CL_) INTERFACE(_CL_)

#define INTRF_END };

#define CAN_USE_BY_POINTER typedef std::shared_ptr<this_type> pointer;
#define CONST_ACCS_POINTER typedef std::shared_ptr<const this_type> pointer_c;

#define INTERFACE_SH(_CL_, _FBRQ_) INTERFACE(_CL_) CAN_USE_BY_POINTER static pointer _FBRQ_;

#define CHLD_INTERFACE(_CL_, _PRNT_) class _CL_: public _PRNT_ {\
	typedef _CL_ this_type;\
	protected: virtual ~_CL_() = default;\
	public:

#define CHLD_INTERFACE_SH(_CL_, _PRNT_) class _CL_: public _PRNT_ {\
	typedef _CL_ this_type;;  CAN_USE_BY_POINTER\
	protected: virtual ~_CL_() = default;\
	public:

template <class ... PrntN>
class PolyChld: public PrntN... {};

#define INHERITED(...) PolyChld<__VA_ARGS__>

#define WITH_TYPE(_NAME_, _DEF_) typedef _DEF_ _NAME_;
#define WITH_ENUM(_NAME_, ...) typedef enum {__VA_ARGS__} _NAME_;
#define WITH_TYPE_PAIR(_NAME_, _1st, _2nd) typedef std::pair<_1st, _2nd> _NAME_;

#define GET_BY(_CLL_) static this_type& _CLL_;
#define GET_CONST_BY(_CLL_) static const this_type& _CLL_;

#define METHOD(_MTD_) virtual _MTD_ = 0;
#define CONST_METHOD(_MTD_) virtual _MTD_ const = 0;
#define WITH_METHOD(_MTD_) METHOD(_MTD_)
#define WITH_CONST_METHOD(_MTD_) CONST_METHOD(_MTD_)
#define CAN_REDEFINE_METHOD(_MTD_) virtual _MTD_;
#define OPERATOR(_RET_TP_, _OP_, _ARG_, ...) \
	virtual _RET_TP_ operator _OP_ (_ARG_) __VA_ARGS__ = 0;
#define WITH_OPERATOR(_OP_) OPERATOR (_OP_)
#define COMPARE_OPERATOR(_OP_, _ARG_) OPERATOR(bool, _OP_, _ARG_, const)
#define SELF_OPERATOR(_OP_, _ARG_, ...) OPERATOR(this_type&, _OP_, _ARG_, __VA_ARGS__)
#define CONST_SELF_OPERATOR( _OP_, _ARG_) OPERATOR(const this_type&, _OP_, _ARG_, const)
#define COMPARE_SAME(_OP_) COMPARE_OPERATOR(_OP_, const this_type&)

#define CAST_TO_TYPE(_T_) OPERATOR(, _T_,, const)

#define PREFIX(_Px) class _Px{protected: virtual ~_Px() = default; public:
#define PFX_END(_Px) }&_Px;

#define INTRF_COMPLETE(_CL_, ...)protected:_CL_(__VA_ARGS__);};

#define OpINPUT(_T_) virtual this_type& operator <<(_T_ &) = 0;
#define OpOUTPUT(_T_) virtual this_type& operator >>(_T_ &) = 0;
#define OPERATION_IO(_T_) OpINPUT(_T_) OpOUTPUT(_T_)

#define HAVE_STUB(_Cll_) static this_type& _Cll_();	\
	CONST_METHOD(bool isStub())

#define ITS_STUB bool isStub() const {return true;}
#define ITS_NO_STUB bool isStub() const {return false;}
//========================CUSTOM=REFERENCE=SUPPORT==============================
#define REFERENCE class ref: public std::reference_wrapper<this_type>{	\
	public:																\
		ref(this_type &rs): std::reference_wrapper<this_type>(rs){};	\
		ref(this_type &&rrs) = delete;									\
		ref(const ref&) = default;										\
		ref& operator= (const ref&) = default;
#define SUPPORT_OPERATOR(_RET_T_, _OP_, ...)	\
		_RET_T_ operator _OP_ (__VA_ARGS__ ref &othr)__VA_ARGS__{		\
			return get() _OP_ othr.get();}
#define REF_END };
//==============================================================================
