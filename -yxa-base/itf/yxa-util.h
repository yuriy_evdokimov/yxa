/*
 * yxa-util.h
 *
 *  Created on: 4 aug 2022 г.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */
#pragma once
#include "yxa-itf-def.h"
#include <set>
#include <signal.h>

namespace yxa{

float nan_flt();

namespace signal{

CAN_BE_DEFINE(handler)
	WITH_METHOD(void onSignal(int))
INTRF_END

void handle(std::set<int>, handler&);
}//namespace signal
}//namespace yxa
