/*	Y-eXtensible Architecture
 *
 *	yxa-itr-def.h
 *
 *  Created on: 22 mar 2016.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */

#pragma once

#include "yxa-itf-def.h"
#include <functional>

//=============================ITERRATION=TOOL==================================
namespace yxa{
template <class _C_>
using iterV = std::function<bool(_C_&, int step, bool isLast)>;

template <class _C_>
using iterC = std::function<bool(const _C_&, int step, bool isLast)>;
}

#define IT_CONTAINS(_CL_) \
	CONST_METHOD(void Iterate(yxa::iterV<_CL_>))
#define IT_CONTAINS_CONST(_CL_) \
	CONST_METHOD(void Iterate(yxa::iterC<_CL_>))

#define IT_CONTAINS_CNT(_CL_) IT_CONTAINS(_CL_) \
		CONST_METHOD(size_t get_##_CL_##_count())

#define IT_CONTAINS_CONST_CNT(_CL_) IT_CONTAINS_CONST(_CL_) \
		CONST_METHOD(size_t get_##_CL_##_count())

#define IT_CONTAINS_AS(_CL_) using _CL_::IterateBy;
#define IT_CONTAINS_WG(_CL_, _GET_) IT_CONTAINS(_CL_)\
		METHOD(std::shared_ptr<_CL_> _GET_)

#define ITR_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)			\
		auto it = _Cnt_.begin();									\
		int _stp = -1;												\
		bool iLast = (_Cnt_.end() == it);							\
		while (!iLast){												\
			iLast = (_Cnt_.end() == ++it);							\
			if (!fi(dynamic_cast<_T_&>(_ObjAccss_ ((*it) _ElmAccs_)),\
													++_stp, iLast))	\
				break;												\
		}

#define REALIZ_ITR_BY_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)		\
		void Iterate(iterV<_T_> fi) const{			                \
					ITR_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)	\
		}

#define REALIZ_KEEP_ITR_BY_STL_CNTNR(_T_, _Cnt_, _MUT_LCKR_, _ElmAccs_, _ObjAccss_)\
		void Iterate(iterV<_T_> fi) const{							\
			std::lock_guard<std::mutex> _keep(_MUT_LCKR_);				\
			ITR_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)			\
		}

#define REALIZ_ITR_C_BY_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)	\
		void Iterate(iterC<_T_> fi) const{			                \
			ITR_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)			\
		}

#define REALIZ_KEEP_ITR_C_BY_STL_CNTNR(_T_, _Cnt_, _MUT_LCKR_, _ElmAccs_, _ObjAccss_)\
		void Iterate(iterC<_T_> fi) const{							\
			std::lock_guard<std::mutex> _keep(_MUT_LCKR_);				\
			ITR_STL_CNTNR(_T_, _Cnt_, _ElmAccs_, _ObjAccss_)			\
		}
//==============================================================================

