/*
 * yxa-util.cpp
 *
 *  Created on: 4 aug 2022 г.
 *  Author: Evdokimov YV (yuriy.v.evdokimov@gmail.com)
 *	LICENSE: GPLv3
 *
 *	git clone from
 *	https://bitbucket.org/yuriy_evdokimov/yxa.git
 */
#include "yxa-util.h"
#include <limits>
#include <mutex>
#include <map>

namespace yxa{

float nan_flt(){return std::numeric_limits<float>::quiet_NaN();}

namespace signal{

static std::mutex hndl_lck;
static std::map<int, handler&> handlers;

static void inHandler(int sg){

	std::lock_guard keep(hndl_lck);
	auto it = handlers.find(sg);

	if (handlers.end() != it)
		it->second.onSignal(sg);
}

void handle(std::set<int> ss, handler &h)
{
	std::lock_guard keep(hndl_lck);
	for (auto sg: ss)
	{
		handlers.insert({sg, h});
		::sigset(sg, inHandler);
	}
}
}
}
